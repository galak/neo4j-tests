package me.sokolenko.neo4j

import Config._
import Util._

/**
 * @author Anatoliy Sokolenko
 */

object RandomReadTest extends TestApp with Test with ParametrizedTest with AssertDbSize {
  override def unitsCount = 100

  def test() {
    (1 until unitsCount).foreach {
      _ =>
        val id = rnd.nextInt(dbSize)
        val node = index.get(ID_PROP, id).getSingle

        node.getProperty(ID_PROP)
    }
  }
}