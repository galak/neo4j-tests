package me.sokolenko.neo4j


/**
 * @author Anatoliy Sokolenko
 */

object NodesPerTransactionTest extends TestApp with Randomized with WriteUtil with PrecleanWriteTest {
  def test() {
    var nodeCounter = 0;
    try {
      val tx = db.beginTx()
      while (true) {
        val parent = getParent(nodeCounter)
        addChildren(nodeCounter, parent)

        nodeCounter += 1
      }

      tx.success() //just to prevent compiler or JIT from removing variable
    } catch {
      case _: OutOfMemoryError => println("Transaction died on " + nodeCounter)
    }
  }
}
