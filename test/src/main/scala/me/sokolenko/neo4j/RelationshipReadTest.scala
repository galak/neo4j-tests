package me.sokolenko.neo4j

import me.sokolenko.neo4j.Config._
import scala.collection.JavaConversions._
import Util._

/**
 * @author Anatoliy Sokolenko
 */

object RelationshipReadTest extends TestApp with Test with Randomized with ParametrizedTest with AssertDbSize {
  override val unitsCount = 100

  def test() {
    (1 until unitsCount).foreach {
      _ =>
        val id = rnd.nextInt(dbSize)
        val node = index.get(ID_PROP, id).getSingle
        node.getRelationships(NodeRelation.Child).foreach {
          _.getNodes.foreach { node =>
            node.getProperty(ID_PROP)
          }
        }
    }
  }
}
