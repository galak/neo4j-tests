package me.sokolenko.neo4j

import me.sokolenko.neo4j.Config._
import scala.collection.JavaConversions._
import Util._

/**
 * @author Anatoliy Sokolenko
 */

object SequentialReadTest extends TestApp with Test with ParametrizedTest with AssertDbSize {
  override def unitsCount = 100

  def test() {
    (1 until unitsCount).foreach {
      _ =>
        val id = rnd.nextInt(dbSize)
        db.getAllNodes.find {
          node =>
            if (node.hasProperty(ID_PROP)) {
              id == node.getProperty(ID_PROP)
            } else false
        }
    }
  }
}
