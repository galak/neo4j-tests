package me.sokolenko.neo4j

/**
 * @author Anatoliy Sokolenko
 */

trait Test {
  private var setUps = Seq[() => Any]()

  def <<(fn: () => Any) {
    setUps +:= fn
  }

  final def setUp() {
    setUps.foreach(_())
  }

  def test()
}

trait ParametrizedTest {
  this: TestApp =>

  def unitsCount: Int = 1
}