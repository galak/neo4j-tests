package me.sokolenko.neo4j

import java.io.File
import me.sokolenko.neo4j.Config._
import org.neo4j.kernel.impl.util.FileUtils
import org.neo4j.graphdb.{GraphDatabaseService, Node}
import org.neo4j.graphdb.index.Index
import Util._

/**
 * @author Anatoliy Sokolenko
 */

object Util {
  val ID_PROP = "id"
}

trait WriteUtil {
  this: DbProvider with Randomized =>

  def addChildren(id: Int, parent: Option[Node] = None) {
    val child = db.createNode()
    child.setProperty(ID_PROP, id)
    index.add(child, ID_PROP, id)

    if (!parent.isEmpty) {
      parent.get.createRelationshipTo(child, NodeRelation.Child)
    }

    for (j <- 1 until 100) {
      val value = rnd.nextLong()

      child.setProperty(String.valueOf(j), value)
      index.add(child, String.valueOf(j), value)
    }
  }

  def getParent(id: Int): Option[Node] = {
    val parentId = (id - 1) / 2

    val parents = index.get(ID_PROP, parentId)
    if (parents.size > 0) {
      Option(parents.getSingle)
    } else {
      None
    }
  }
}

trait AssertDbSize extends Test {
  this: DbProvider =>

  def ensureNodesCount() {
    db.getNodeById(dbSize)
  }

  <<(ensureNodesCount _)
}

/**
 * Convention about service level that should be provided by class in order to
 * make [[me.sokolenko.neo4j.WriteUtil]] work.
 */
trait DbProvider {
  protected def db: GraphDatabaseService

  protected def index: Index[Node]
}

trait PrecleanWriteTest extends Test {
  def ensureDbClean() {
    val dbDir = new File(dbLocation)
    if (dbDir.exists()) {
      FileUtils.deleteRecursively(dbDir)
    }
  }

  <<(ensureDbClean _)
}