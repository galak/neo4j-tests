package me.sokolenko.neo4j

import org.neo4j.kernel.EmbeddedGraphDatabase
import me.sokolenko.neo4j.Config._
import util.Random
import java.lang.System

/**
 * @author Anatoliy Sokolenko
 */

abstract class TestApp extends App with DbProvider with Randomized {
  this: Test =>

  protected implicit lazy val db = new EmbeddedGraphDatabase(dbLocation)

  protected implicit lazy val index = db.index().forNodes("nodes")

  Config(args)

  setUp()

  val start = System.currentTimeMillis

  test()

  val end = System.currentTimeMillis
  printf("Total time = %s msec\n", (end - start))
  val _unitsCount = this match {
    case t: ParametrizedTest => t.unitsCount
    case _ => 1
  }

  if (_unitsCount > 1) {
    printf("Time per unit = %f msec, units processed = %s\n", (end - start).toDouble / _unitsCount, _unitsCount)
  }

  db.shutdown()
}

trait Randomized {
  protected lazy val rnd = new Random
}