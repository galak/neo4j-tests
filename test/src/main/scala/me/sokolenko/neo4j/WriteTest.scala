package me.sokolenko.neo4j

import Config._

/**
 * @author Anatoliy Sokolenko
 */

object WriteTest extends TestApp with Randomized with WriteUtil with PrecleanWriteTest {
  def test() {
    var nodeCounter = 0;
    while (nodeCounter < dbSize) {
      val tx = db.beginTx()
      try {
        (0 until maxNodePerTx).foreach {
          _ =>
            val parent = getParent(nodeCounter)
            addChildren(nodeCounter, parent)

            nodeCounter += 1
        }

        tx.success()

        printf("Created %s nodes, at %s msec\n", nodeCounter, (System.currentTimeMillis - start))
      } finally tx.finish()
    }
  }
}