package me.sokolenko.neo4j

import org.neo4j.graphdb.RelationshipType

/**
 * @author Anatoliy Sokolenko
 */

object NodeRelation {
  val Child = new RelationshipType {
    def name() = "child"
  }
}

