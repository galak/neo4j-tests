package me.sokolenko.neo4j

/**
 * @author Anatoliy Sokolenko
 */

object Config {
  private var _args: Option[Array[String]] = _

  def apply(args: Array[String]) {
    if (args.size < 3) {
      throw new IllegalArgumentException("Specify 3 parameters")
    }

    _args = Some(args)
  }

  private def args(id: Int) = _args match {
    case _: Some[_] => _args.get(id)
    case _ => throw new IllegalStateException("Initialize configuration first")
  }

  lazy val dbSize: Int = args(0).toInt

  lazy val dbLocation: String = args(1)

  lazy val maxNodePerTx: Int = args(2).toInt
}