#!/bin/sh

trap 'exit 1' 2         # trap Ctrl-C (signal 2)

DB_SIZE=$1
DB_LOCATION=$2
TX_SIZE=$3

if [ -z "$DB_SIZE" ]; then
    echo "Please specify size of database for test" 1>&2
    exit 1
fi

if [ -z "$DB_LOCATION" ]; then
    DB_LOCATION=`mktemp -d -t neo4jtest`
fi

if [ -z "$TX_SIZE" ]; then
    TX_SIZE="1000"
fi

CD=`dirname $0`
LIB=$CD/../lib
JAVA_OPTS="-XX:MaxPermSize=256m -Xmx3g -XX:+UseConcMarkSweepGC"
TEST_OPTS="$DB_SIZE $DB_LOCATION $TX_SIZE"

for i in $LIB/*.jar; do
    CLASSPATH=$CLASSPATH:$i
done
CLASSPATH=`echo $CLASSPATH | cut -c2-`

run() {
    DESC=$1
    MAIN_CLASS=$2

    echo
    echo $DESC
    java -cp $CLASSPATH $JAVA_OPTS $MAIN_CLASS $TEST_OPTS 2>&1 | sed 's/^/   /;s/$//'
}

run "Write"                 "me.sokolenko.neo4j.WriteTest"
run "Random Read"           "me.sokolenko.neo4j.RandomReadTest"
run "Relationship Read"     "me.sokolenko.neo4j.RelationshipReadTest"
run "Sequential Read"       "me.sokolenko.neo4j.SequentialReadTest"
run "Nodes per Transaction" "me.sokolenko.neo4j.NodesPerTransactionTest"